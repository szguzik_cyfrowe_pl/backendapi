<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('test', [\App\Http\Controllers\ApiController::class, 'index']);
Route::post('login', [\App\Http\Controllers\AuthController::class, 'login']);
Route::post('register', [\App\Http\Controllers\AuthController::class, 'register']);
Route::post('forgot', [\App\Http\Controllers\ForgotController::class, 'forgot']);
Route::post('reset', [\App\Http\Controllers\ForgotController::class, 'reset']);
Route::get('user', [\App\Http\Controllers\AuthController::class, 'user'])->middleware('auth:api');
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
