<?php

namespace App\Http\Controllers;

use App\Http\Requests\ForgotRequest;
use App\Http\Requests\ResetRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;


class ForgotController extends Controller
{
    public function forgot(ForgotRequest $request)
    {
        $email = $request->input('email');

        if (User::where('email', $email)->doesntExist()) {
            return response(['message' => "User doesn't exists!"], 404);
        }

        $token = Str::random(10);
        try {
            DB::table('password_resets')->insert([
                'email' => $request->input('email'),
                'token' => $token,
            ]);

           Mail::send('Mails.forgot', ['token' => $token], function(Message $message) use ($email) {
                $message->to($email);
                $message->subject('Reset Twojego hasła');
                $message->from('system@localhost.local');
           });

            return response(['message' => 'Check email'], 200);
        } catch (\Exception $exception) {
            return response(['message' => $exception->getMessage()], 401);
        }
    }

    public function reset(ResetRequest $request) {

        $token = $request->input('token');

        if (!$passwordResets = DB::table('password_resets')->where('token', $token)->first()) {
            return response(['message' => 'Invalid Token ! kuźwa ....'], 400);
        }

        /** @var User $user */
        if (! $user = User::where('email', $passwordResets->email)->first()) {
            return response(['message' => "Email doesn't exist kuźwa"], 404);
        }

        $user->password = Hash::make($request->input('password'));
        $user->save();

        if ($user) {
            DB::table('password_resets')->where('token', $token)->delete();
        }


        return response(['message' => 'success'], 200);
    }
}
